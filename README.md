# BLUE STEEL

An app which lets you keep track of tasks that you are working on and helps motivate you to get them done. 
The project is currently in Alpha testing and is not a finished product.

# Installing
* Unzip and run blue-steel.exe
* If you get a security message from Windows when you try to run the app you should select More info -> Run anyway. This software is 100% safe and is simply unknown to Windows.*
* You may also get a security message from you anti-virus software, in which case you should add the app to the safe list.

Copyright © 2018 J.L.P.C All Rights Reserved


